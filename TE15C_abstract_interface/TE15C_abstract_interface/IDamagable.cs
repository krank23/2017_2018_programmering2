﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_abstract_interface
{
    interface IDamagable
    {
        void TakeDamage(int amount);
    }
}
