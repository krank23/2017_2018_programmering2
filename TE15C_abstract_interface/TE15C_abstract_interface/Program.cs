﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_abstract_interface
{
    class Program
    {
        static void Main(string[] args)
        {
            Owlbear steve = new Owlbear();

            RandomDamageTo(steve);

        }

        static void RandomDamageTo( IDamagable thing)
        {
            Random rnd = new Random();

            thing.TakeDamage(rnd.Next(1, 5));
        }

    }
}
