﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_abstract_interface
{
    abstract class Character : IDamagable, IHealable
    {
        public int hp = 100;

        public abstract int Attack();

        public void Heal(int amount)
        {
            hp += amount;
        }

        public void TakeDamage(int amount)
        {
            hp -= amount;
        }
    }
}
