﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_arv
{
    class Character
    {
        public string name;
        public int hp;

        public int minDamage;
        public int maxDamage;


        public virtual int Attack()
        {
            return (minDamage + maxDamage) / 2;
        }

    }
}
