﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_arv
{
    class Hero : Character
    {
        public int xp = 0;
        public int level = 1;

        static Random r = new Random();

        public Hero()
        {
            name = "Jeff";
            hp = 250;

            minDamage = 5;
            maxDamage = 10;

        }

        public override int Attack()
        {
            return r.Next(minDamage, maxDamage);
        }

    }
}
