﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileGenerator : MonoBehaviour {

    public GameObject tile;

    public int width = 10;
    public int height = 10;

	// Use this for initialization
	void Start () {

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                PutTile(x, y);
            }
        }
	}

    protected virtual void PutTile(int x, int y)
    {
        GameObject newTile = Instantiate(tile);

        newTile.transform.SetParent(this.transform);

        newTile.transform.localPosition = new Vector3(x, y) * 0.5f;
    }

}
