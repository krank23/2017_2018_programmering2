﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGenerator : TileGenerator
{

    protected override void PutTile(int x, int y)
    {
        /*if (x == width/2 && (y == height-1 || y == 0))
        {
            base.PutTile(x, y);
        }
        else if (y == height/2 && (x == width-1 || x == 0))
        {
            base.PutTile(x, y);
        }*/

        /*if (y % 4 == x % 4)
        {
            base.PutTile(x, y);
        }*/

        /*if (x % 2 == 1 && (y != 0 && y != height-1))
        {
            base.PutTile(x, y);
        }
        else if (x % 4 == 2 && y == height -2)
        {
            base.PutTile(x, y);
        }
        else if (x % 4 == 0 && y == 1)
        {
            base.PutTile(x, y);
        }*/

        if (x == 0 || y == 0 || x == width - 1 || y == height - 1)
        {
            base.PutTile(x, y);
        }
        else if( x % 2 == 0 && y % 2 == 0)
        {
            base.PutTile(x, y);
        }

    }
}
