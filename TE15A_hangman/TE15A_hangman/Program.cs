﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_hangman
{
    class Program
    {
        static void Main(string[] args)
        {
            // Välja ett ord
            string word = "";
            while (word.Length <= 2 || word.Contains(" "))
            {
                Console.Write("Please type a word: ");
                word = Console.ReadLine().ToLower();
            }

            string[] underscores = new string[word.Length];

            for (int i = 0; i < underscores.Length; i++)
            {
                underscores[i] = "_";
            }

            int chancesLeft = 10;
            
            while (chancesLeft > 0 && underscores.Contains("_"))
            {
                Console.Clear();

                Console.WriteLine(String.Join(" ", underscores));

                string guess = "";
                while (guess.Length != 1 || word.Contains(" "))
                {
                    Console.Write("Type ur guess (1 ltr only): ");
                    guess = Console.ReadLine().Trim();
                }
                guess = guess.ToLower();

                if (word.Contains(guess))
                {
                    int where = word.IndexOf(guess);
                    underscores[where] = guess;
                    Console.WriteLine("yay");
                }
                else
                {
                    chancesLeft--;
                    Console.WriteLine("nay");
                }
            }

            if (chancesLeft > 0)
            {
                Console.WriteLine("CONGRATIONS");
            }
            else {
                Console.WriteLine("SADFACE");
            }

            Console.WriteLine("Game over");

            Console.ReadLine();
        }
    }
}
