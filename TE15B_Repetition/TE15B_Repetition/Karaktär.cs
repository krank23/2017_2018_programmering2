﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_Repetition
{
    class Karaktär : Flyttbara
    {
        public int hp;

        public Karaktär()
        {
            hp = 10;
            Console.WriteLine("Haj på daj!");
        }

        public void Hurt(int amount)
        {
            hp -= amount;
        }

        public void Attack(Karaktär k)
        {
            k.Hurt(1);
        }

    }
}
