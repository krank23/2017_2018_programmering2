﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_Repetition
{
    class Flyttbara
    {
        public float x = 0;
        public float y = 0;
        public float z = 0;
        public float maxSpeed = 1;
        public float rotX = 0;
        public float rotY = 0;
        public float rotZ = 0;

        public float speedX = 0.4f;

        public void Update()
        {
            x += speedX;
        }
    }
}
