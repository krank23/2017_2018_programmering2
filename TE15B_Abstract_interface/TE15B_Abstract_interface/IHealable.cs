﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_Abstract_interface
{
    interface IHealable
    {
        void Heal(int amount);
    }
}
