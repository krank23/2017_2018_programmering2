﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_Abstract_interface
{
    class Program
    {
        static void Main(string[] args)
        {
            NaziZombie jeff = new NaziZombie();


            RandomDamageTo(jeff);
        }

        static void RandomDamageTo(IHurtable thing)
        {
            Random rnd = new Random();
            thing.Hurt(rnd.Next(3, 6));
        }
    }
}
