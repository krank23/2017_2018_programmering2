﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    [SerializeField]
    string axis;

    float speed = 6f;

    // Update is called once per frame
    void Update()
    {

        float moveY = Input.GetAxisRaw(axis);
        Vector2 movement = new Vector2(0, moveY) * speed * Time.deltaTime;

        transform.Translate(movement);

    }
}
