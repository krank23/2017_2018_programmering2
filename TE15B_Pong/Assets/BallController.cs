﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour
{

    float force = 51;
    int leftPoints = 0;

    [SerializeField]
    Text leftPointsText;

    // Use this for initialization
    void Start()
    {
        ResetBall();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "GoalRight")
        {
            leftPoints++;
            leftPointsText.text = leftPoints.ToString();
        }


        ResetBall();

    }

    private void ResetBall()
    {
        transform.position = Vector3.zero;

        Rigidbody2D rb = this.GetComponent<Rigidbody2D>();

        rb.velocity = Vector3.zero;

        Vector2 v = new Vector2(-1, 1) * force;

        rb.AddForce(v);
    }

}
