﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour {

    [SerializeField]
    float speed = 2;

    [SerializeField]
    string axis;

	
	// Update is called once per frame
	void Update () {

        float moveY = Input.GetAxisRaw(axis);

        Vector3 movement = new Vector3(0, moveY, 0) * Time.deltaTime * speed;

        transform.Translate(movement);

        if (transform.position.y > 4 || transform.position.y < -4)
        {
            transform.Translate(-movement);
        }


	}
}
