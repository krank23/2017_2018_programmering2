﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour {

    int points = 0;

    [SerializeField]
    float forceMultiplier;

    [SerializeField]
    Text pointsText;

	void Start () {

        Restart();

	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        points++;
        pointsText.text = points.ToString();

        Restart();
    }

    private void Restart()
    {
        transform.position = Vector3.zero;

        Vector2 force = Vector2.up + Vector2.left;

        force *= forceMultiplier;

        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = Vector3.zero;

        rb.AddForce(force);
    }


}
