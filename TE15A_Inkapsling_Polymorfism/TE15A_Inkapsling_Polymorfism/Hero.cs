﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_Inkapsling_Polymorfism
{
    class Hero
    {
        private int hp = 100;
        private int maxHp = 100;

        private int xp = 0;
        
        public void AddXp(int amount)
        {
            amount = Math.Max(0, amount);
            xp += amount;
        }

        public virtual void AddXp()
        {
            AddXp(1);
        }

        public int GetXp()
        {
            return xp;
        }

        public int GetLevel()
        {
            return xp / 10;
        }

        public void SetHp(int value)
        {
            hp = value;
            hp = Math.Max(0, hp);
            hp = Math.Min(maxHp, hp);
        }

        public int GetHp()
        {
            return hp;
        }

    }
}
