﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_Inkapsling_Polymorfism
{
    class Warrior : Hero
    {
        public int test = 666;

        public override void AddXp()
        {
            AddXp(5);
        }
    }
}
