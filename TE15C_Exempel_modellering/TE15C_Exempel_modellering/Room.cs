﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_Exempel_modellering
{
    class Room
    {
        public string description;
        public enum directions
        {
            north, east, south, west
        }
        
        Dictionary<directions, Room> adjacentRooms;

        public Room(string d)
        {
            description = d;
            adjacentRooms = new Dictionary<directions, Room>();
        }

        public void setAdjacentRoom(directions direction, Room room)
        {
            adjacentRooms[direction] = room;
        }

        public Room AskWhereNext()
        {
            directions[] availableDirections = adjacentRooms.Keys.ToArray();

            for (int i = 0; i < availableDirections.Length; i++)
            {
                Console.WriteLine((i+1) + ". " + availableDirections[i]);
            }

            int n = -1;
            string a = "";
            while (a == "" || !a.All(char.IsNumber) || n > availableDirections.Length || n < 1)
            {
                a = Console.ReadLine();
                if (a != "" && a.All(char.IsNumber))
                {
                    n = int.Parse(a);
                }
            }

            directions dir = availableDirections[n - 1];

            Console.WriteLine("You go " + availableDirections[n-1]);

            return adjacentRooms[dir];
        }

    }
}
