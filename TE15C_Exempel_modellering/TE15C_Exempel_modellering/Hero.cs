﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_Exempel_modellering
{
    class Hero
    {
        public int hp;
        public string name;
        public float size = 3.5f;
        public int xp;
        public int level;
        static Random generator = new Random();
        int x = 5;
        int direction = 1;

        public Hero(string n, int lvl)
        {
            hp = 100;
            name = n;
            xp = 0;
            level = lvl;
            Console.WriteLine("IMALIVE");
        }


        public int Attack()
        {
            return generator.Next(6,11) + level;
        }

        public void Hurt(int amount)
        {
            hp -= amount;
        }

        public void Movement()
        {
            x += direction;

            if (x > 10 || x < 0)
            {
                direction = -direction;
            }
        }

    }
}
