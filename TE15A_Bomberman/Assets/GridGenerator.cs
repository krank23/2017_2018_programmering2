﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour {

    public GameObject tilePrefab;

    public int width = 5;
    public int height = 3;

    float tileSize = 0.5f;


    // Use this for initialization
    void Start () {

        //float aspect = (float)Screen.width / Screen.height;

        //print(Camera.main.aspect);

        int w = Mathf.CeilToInt((Camera.main.orthographicSize * 2 * Camera.main.aspect)/tileSize);
        int h = Mathf.CeilToInt((Camera.main.orthographicSize * 2)/tileSize);

        // Make sure they are odd
        if (w % 2 == 0)
        {
            w++;
        }
        if (h % 2 == 0)
        {
            h++;
        }

        print("width: " + w);
        print("Height: " + h);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                PutTile(x, y);
            }
        }
	}

    public virtual void PutTile(int x, int y)
    {
        Vector3 position = new Vector3(x, y) * tileSize;

        GameObject newTile = Instantiate(tilePrefab);
        newTile.transform.SetParent(this.transform);

        newTile.transform.localPosition = position;
    }
	
}
