﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_arv
{
    class Enemy: Character
    {
        public Enemy() {
            hp = 50;
            name = "Rolf";
            minDamage = 5;
            maxDamage = 10;
        }

    }
}
