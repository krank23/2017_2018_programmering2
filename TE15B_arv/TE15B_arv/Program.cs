﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_arv
{
    class Program
    {
        static void Main(string[] args)
        {
            Hero h1 = new Hero();
            Enemy e1 = new Enemy();

            int dmg = e1.Attack();

            Console.WriteLine(dmg);

            Console.ReadLine();
        }
    }
}
