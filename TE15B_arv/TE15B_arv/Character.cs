﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_arv
{
    class Character
    {
        public int hp;
        public string name;
        public int minDamage;
        public int maxDamage;

        public virtual int Attack()
        {
            return (minDamage + maxDamage) / 2;
        }

    }
}
