﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_arv
{
    class Hero: Character
    {
        static Random generator = new Random();

        public Hero()
        {
            hp = 100;
            name = "Eva-Lena";
            minDamage = 10;
            maxDamage = 20;
        }

        public override int Attack()
        {
            

            return generator.Next(minDamage, maxDamage);
        }

    }
}
