﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_NumberPrio
{
    class Program
    {
        static void Main(string[] args)
        {
            int p1pts = 20;
            int p2pts = 20;

            int[] p1values = new int[5];
            int[] p2values = new int[5];

            Console.WriteLine("Spelare 1!");

            string ps;
            for (int i = 0; i < p1values.Length; i++)
            {
                Console.WriteLine("Hur många poäng vill du spendera på värde #" + (i+1) + "/5?");
                Console.WriteLine("Du har " + p1pts + " poäng kvar");
                ps = Console.ReadLine();

                int p = int.Parse(ps);
                p = Math.Min(p, p1pts);

                p1pts -= p;
                p1values[i] = p;
            }

            Console.Clear();
            Console.WriteLine("Spelare 2!");

            for (int i = 0; i < p2values.Length; i++)
            {
                Console.WriteLine("Hur många poäng vill du spendera på värde #" + (i + 1) + "/5?");
                Console.WriteLine("Du har " + p2pts + " poäng kvar");
                ps = Console.ReadLine();

                int p = int.Parse(ps);

                p = Math.Min(p, p2pts);

                p2pts -= p;
                p2values[i] = p;
            }
            Console.Clear();

            int p1wins = 0;
            int p2wins = 0;

            Console.WriteLine("P1:   P2:");
            for (int i = 0; i < p1values.Length; i++)
            {
                Console.Write(p1values[i] + " : " + p2values[i]);
                if (p1values[i] > p2values[i])
                {
                    Console.WriteLine(" vinnare: Spelare 1");
                    p1wins++;
                }
                else if (p1values[i] < p2values[i])
                {
                    Console.WriteLine(" vinnare: Spelare 2");
                    p2wins++;
                }
                else
                {
                    Console.WriteLine("oavgjort");
                }
            }

            

            if (p1wins > p2wins)
            {
                Console.WriteLine("Spelare 1 vinner!");
            }
            else if (p1wins < p2wins)
            {
                Console.WriteLine("Spelare 2 vinner!");
            }
            else
            {
                Console.WriteLine("Oavgjort!");
            }


            Console.ReadLine();
        }
    }
}
