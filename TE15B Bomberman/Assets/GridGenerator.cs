﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour {

    [SerializeField]
    GameObject somePrefab;

    [SerializeField]
    GameObject tilePrefab;

    [SerializeField]
    protected int sizeX = 5;

    [SerializeField]
    protected int sizeY = 5;

    protected GameObject[,] tiles;

	// Use this for initialization
	void Start () {

        tiles = new GameObject[sizeX, sizeY];

        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                PutTile(x, y);
            }
        }
	}

    protected virtual void PutTile(int x, int y)
    {
        float xMod = 0.5f + (-sizeX / 2.0f);
        float yMod = 0.5f + (-sizeY / 2.0f);

        Vector3 newPos = new Vector3(x + xMod, y + yMod, 0) * 0.5f;
        newPos.z = transform.position.z;

        GameObject newTile = (GameObject) Instantiate(tilePrefab, newPos, Quaternion.identity);

        newTile.transform.SetParent(this.transform);
        tiles[x, y] = newTile;
    }
}
