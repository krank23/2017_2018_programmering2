﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGenerator : GridGenerator {

    protected override void PutTile(int x, int y)
    {

        if (x == 0 || x == sizeX-1 || y == 0 || y == sizeY-1)
        {
            base.PutTile(x, y);
        }

        else if (x % 2 == 0 && y % 2 == 0)
        {
            base.PutTile(x, y);
        }
        
    }

}
