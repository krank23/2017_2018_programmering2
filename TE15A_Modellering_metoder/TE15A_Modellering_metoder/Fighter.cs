﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_Modellering_metoder
{
    class Fighter
    {
        public int minDamage = 5;
        public int maxDamage = 10;

        private string[] inventory = new string[4];
       
        

        private int hp = 100;

        public Fighter()
        {


            Console.WriteLine("IMALIVE");
        }

        public int Attack()
        {
            Random generator = new Random();

            int dmg = generator.Next(minDamage, maxDamage);

            return dmg;
        }

        public void Hurt(int amount)
        {
            hp -= amount;
        }



    }
}
