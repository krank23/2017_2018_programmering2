﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_Modellering_metoder
{
    class Program
    {
        static void Main(string[] args)
        {
            Fighter f1 = new Fighter();

            int d = f1.Attack();

            f1.Hurt(d);


            Console.WriteLine(d);

            Console.ReadLine();
        }
    }
}
