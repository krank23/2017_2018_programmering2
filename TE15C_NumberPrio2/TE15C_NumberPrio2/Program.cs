﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_NumberPrio2
{
    class Program
    {
        static void Main(string[] args)
        {
            int p1points = 20;
            int p2points = 20;

            int[] p1places = new int[5];
            int[] p2places = new int[5];

            Console.WriteLine("Spelare 1!");
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Du har " + p1points + " kvar");
                Console.WriteLine("Skriv in hur många poäng du vill lägga på plats " + (i + 1) + ": ");
                string p = Console.ReadLine();

                int pint = int.Parse(p);

                p1points -= pint;
                p1places[i] = pint;
            }

            Console.Clear();

            Console.WriteLine("Spelare 2!");
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Du har " + p2points + " kvar");
                Console.WriteLine("Skriv in hur många poäng du vill lägga på plats " + (i + 1) + ": ");
                string p = Console.ReadLine();

                int pint = int.Parse(p);

                p2points -= pint;
                p2places[i] = pint;
            }

            int p1wins = 0;
            int p2wins = 0;

            for (int i = 0; i < p1places.Length; i++)
            {
                Console.WriteLine("Position: " + (i + 1));
                Console.WriteLine("P1: " + p1places[i]);
                Console.WriteLine("P2: " + p2places[i]);

                if (p1places[i] > p2places[i])
                {
                    Console.WriteLine("p1 wins!");
                    p1wins++;
                }
                else if (p1places[i] < p2places[i])
                {
                    Console.WriteLine("p2 wins!");
                    p2wins++;
                }
                else
                {
                    Console.WriteLine("Oavgjort!");
                }
            }

            if (p1wins > p2wins)
            {
                Console.WriteLine("p1 is winner!");
            }
            else if (p1wins < p2wins)
            {
                Console.WriteLine("p2 is winner!");
            }
            else
            {
                Console.WriteLine("neither is winner!");
            }

            Console.ReadLine();
        }
    }
}
