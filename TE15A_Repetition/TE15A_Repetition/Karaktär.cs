﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_Repetition
{
    class Karaktär
    {
        public int hp;
        public float x;
        public float y;
        public float z;
        public float speedX;

        public Karaktär()
        {
            hp = 10;
            x = 0;
            y = 0;
            z = 0;
            speedX = 3;
        }

        public void Update()
        {
            x += speedX;
        }

        public void Hurt(int amount)
        {
            hp -= amount;
        }

        public void Attack(Karaktär k)
        {
            k.Hurt(2);
        }


    }
}
