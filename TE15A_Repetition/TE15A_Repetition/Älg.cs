﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_Repetition
{
    class Älg : Karaktär
    {
        public Älg()
        {
            Demonstrate();
        }

        public void Demonstrate()
        {
            Console.WriteLine("Älgarna har fått nog!");
            Console.WriteLine("I skogen kan ingen höra dig skrika!");
        }
    }
}
