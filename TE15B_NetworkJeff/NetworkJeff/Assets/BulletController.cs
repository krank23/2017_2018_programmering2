﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public float initialVelocity = 6;
    public float lifeTime = 4;
	
	void Start () {
        Rigidbody rb = GetComponent<Rigidbody>();

        rb.velocity = transform.forward * initialVelocity;
	}

    private void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime < 0)
        {
            Destroy(this.gameObject);
        }
    }
}
