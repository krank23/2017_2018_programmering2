﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {

    public float movementSpeed = 3f; // Unity units per second
    public float rotationSpeed = 150f; // Degrees per second

    public GameObject bulletPrefab;
    public Transform bulletSpawnPoint;

    float timeBetweenShots = 0.5f; // Seconds between shots
    float timeSinceLastShot = 0f;
    
    public override void OnStartLocalPlayer()
    {
        GetComponent<Renderer>().material.color = Color.green;
    }

    void Update () {

        if (!isLocalPlayer)
        {
            return;
        }

        float yRotation = Input.GetAxisRaw("Horizontal") * rotationSpeed * Time.deltaTime;
        float zMovement = Input.GetAxisRaw("Vertical") * movementSpeed * Time.deltaTime;

        Vector3 rotationVector = new Vector3(0, yRotation, 0);
        Vector3 movementVector = new Vector3(0, 0, zMovement);

        transform.Rotate(rotationVector);
        transform.Translate(movementVector);

        timeSinceLastShot += Time.deltaTime;
        
        if (Input.GetAxisRaw("Fire1") > 0)
        {
            if (timeSinceLastShot > timeBetweenShots) {
                CmdFire();
                timeSinceLastShot = 0;
            }
        }

	}

    [Command]
    void CmdFire()
    {
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        NetworkServer.Spawn(bullet);
    }

}
