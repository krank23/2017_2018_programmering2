﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour {

    [SerializeField]
    string axis;

    float speed = 10f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        float moveY = Input.GetAxisRaw(axis);

        Vector2 movement = new Vector2(0, moveY) * speed * Time.deltaTime;

        transform.Translate(movement);

        if (transform.position.y > 4 || transform.position.y < -4)
        {
            transform.Translate(-movement);
        }

	}
}
