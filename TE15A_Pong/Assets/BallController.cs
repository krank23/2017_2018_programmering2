﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour {

    float force = 150f;

    [SerializeField]
    Text leftPointsText;

    int leftPoints = 0;

	// Use this for initialization
	void Start () {

        ResetPosition();

	}


	void ResetPosition()
    {
        this.transform.position = Vector3.zero;

        Vector2 forceVector = new Vector2(-1, 1) * force;

        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        rb.velocity = Vector3.zero;


        rb.AddForce(forceVector);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Right")
        {
            leftPoints++;
            leftPointsText.text = leftPoints.ToString();
        }

        
        ResetPosition();
    }
}
