﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_Abstract_interface
{
    class Program
    {
        static void Main(string[] args)
        {
            Charizard steve = new Charizard();

            RandomDamageTo(steve);
        }

        static void RandomDamageTo(Damagable thing)
        {
            Random r = new Random();
            thing.TakeDamage(r.Next(3, 6));
        }
    }
}
