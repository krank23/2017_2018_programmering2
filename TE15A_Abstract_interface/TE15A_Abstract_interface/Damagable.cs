﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_Abstract_interface
{
    interface Damagable
    {
        void TakeDamage(int amount);
    }
}
