﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_Abstract_interface
{
    abstract class Character : Damagable, Healable
    {
        protected int hp = 100;

        abstract public int Attack();

        public void Heal(int amount)
        {
            
        }

        public void TakeDamage(int amount)
        {
            
        }
    }
}
