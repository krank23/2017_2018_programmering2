﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_Modellering
{
    class Fighter
    {
        int hp = 40;
        int x;
        int y;
        int points;
        int minDamage = 5;
        int maxDamage = 10;
        public string name;

        static private Random generator = new Random();

        public Fighter(string n)
        {
            name = n;

            Console.WriteLine("IM ALIVE");
            hp = generator.Next(40, 60);
            Console.WriteLine("MY HP IS " + hp);
        }

        public Fighter()
        {
            name = "Dumhuvud";

            Console.WriteLine("IM ALIVE");
            hp = generator.Next(40, 60);
            Console.WriteLine("MY HP IS " + hp);
        }



        public int Attack()
        {
            return Attack(0);
        }

        public int Attack(int bonus)
        {
            Random generator = new Random();

            int dmg = generator.Next(minDamage, maxDamage) + bonus;

            return dmg;
        }


        public void Hurt(int amount)
        {
            hp -= amount;
        }

    }
}
