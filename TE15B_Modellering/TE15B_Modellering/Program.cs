﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TE15B_Modellering
{
    class Program
    {
        static void Main(string[] args)
        {
            Fighter f1 = new Fighter();
            Fighter f2 = new Fighter("Eva-Lena");


            int d = f1.Attack(10);

            f2.Hurt(d);

            Console.ReadLine();
        }
    }
}
