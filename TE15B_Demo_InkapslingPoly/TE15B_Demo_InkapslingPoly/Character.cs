﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_Demo_InkapslingPoly
{
    class Character
    {
        int hp = 35;

        public int Hp
        {
            get
            {
                return hp;
            }

            set
            {
                hp = value;
                if (hp < 0)
                {
                    hp = 0;
                }
            }
        }

        public void SetHp(int value)
        {
            hp = value;
            if (hp < 0)
            {
                hp = 0;
            }
        }

        public int GetHp()
        {
            return hp;
        }

        public void Hurt()
        {
            Hurt(1);
        }

        public void Hurt(int amount)
        {
            hp -= amount;
        }

    }
}
