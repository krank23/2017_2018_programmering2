﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15B_Hangman
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.BackgroundColor = ConsoleColor.Green;
            Console.ResetColor();


            string word = "abrakadabra";
            int chancesLeft = 7;

            string[] underscores = new string[word.Length];

            for (int i = 0; i < underscores.Length; i++)
            {
                underscores[i] = "_";
            }

            while (chancesLeft > 0 && underscores.Contains("_"))
            {
                Console.WriteLine(String.Join(" ", underscores));

                Console.WriteLine("Chances left: " + chancesLeft);

                string guess = "";
                while (guess.Length != 1 || guess == " ")
                {
                    Console.WriteLine("Guess a letter: ");
                    guess = Console.ReadLine();
                }

                Console.Clear();

                if (word.Contains(guess))
                {

                    Console.WriteLine("yay");
                    //int place = word.IndexOf(guess);
                    //underscores[place] = guess;

                    for (int i = 0; i < word.Length; i++)
                    {
                        // Hämta char vid plats i, konvertera till string, jämför
                        if (word[i].ToString() == guess)
                        {
                            underscores[i] = guess;
                        }
                    }

                }
                else
                {
                    Console.WriteLine("nay");
                    chancesLeft--;
                }

            }

            Console.WriteLine("Ordet var " + word + "!");

            if (chancesLeft > 0)
            {
                Console.WriteLine("WINNER");
            }
            else
            {
                Console.WriteLine("LOSER");
            }

            Console.ReadLine();

        }
    }
}
