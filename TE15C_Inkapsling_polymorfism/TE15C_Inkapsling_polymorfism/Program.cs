﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_Inkapsling_polymorfism
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Monster monster = new Monster("HUGO");

            Console.WriteLine(monster.GetHp());

            Kraken k = new Kraken();

            Monster m = new Kraken();

            


            Monster[] monsterList = new Monster[2];

            monsterList[0] = new Monster();
            monsterList[1] = new Kraken();

            Console.ReadLine();
        }



    }
}
