﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15C_Inkapsling_polymorfism
{
    class Monster
    {
        private int hp;

        private int xp;
        private int level;

        public Monster()
        {
            hp = 100;
            xp = 0;
            level = 0;
        }

        public Monster(int h)
        {
            hp = h;
        }

        public Monster(string name)
        {
            Console.WriteLine(name + "says HELLO HUMANS WHAT A NICE DAY");
        }

        public int GetHp()
        {
            return hp;
        }

        public void SetHp(int value)
        {
            hp = value;
            if (hp < 0)
            {
                hp = 0;
            }

        }

        public void AddXp(int value)
        {
            if (value > 0)
            {
                xp += value;
                level = xp / 5;
            }
        }

        public void AddXp()
        {
            AddXp(1);
        }

        public int GetLevel()
        {
            return level;
        }



    }
}
